import React, {useState, useEffect} from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import MainPage from './views/MainPage';
import RoomPage from './views/RoomPage';
import ModalBox from './components/ModalBox';
import Login from './components/Login';
import Registration from './components/Registration';
import News from './components/News';

function App() {

  const[modalBox, setModalBox] = useState(false)

  const[IsAuth, setIsAuth] = useState(false)

  useEffect(() => {
    const token = localStorage.getItem('token')

    if(token){
      setIsAuth(true)
    }else{
      setIsAuth(false)
    }
  }, [])

  return (
    <div className="App">
      <Header setModalBox={setModalBox} />
      <Routes>
        <Route path='/' element={ <MainPage IsAuth={IsAuth} /> } />
        <Route path='/room' element={ <RoomPage /> } />
      </Routes>
      {
        modalBox
          ?
            <ModalBox setModalBox={setModalBox}>
              <Login />
              <Registration/>
            </ModalBox>
          :
            null
      }
    </div>
  );
}

export default App;
