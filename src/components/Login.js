import React from 'react';

function Login() {
  function Log()
  {
    const login = document.getElementById('login').value
    const password = document.getElementById('password').value
    
    const data = 
    {
      login: login,
      password: password
    }

    const api = 'http://localhost:9001/login'

    fetch(api, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(result => result.json())
    .then((result) => {
      localStorage.setItem('token', result.token)
      console.log(result.token)
    })

    
  }
  return (
    <>
        <h1>Вход на сайт</h1>
        <input id='login' type='text' placeholder='Введите логин' />
        <input id='password' type='password' placeholder='Введите пароль' />
        <button onClick={Log}>Войти</button>
    </>
  );
}

export default Login;
