import React, {useState, useEffect} from 'react';
import Post from './Post';
import NewsFormAdd from './NewsFormAdd';
import './News.css';
import Login from './Login';

function News({IsAuth}) {
  
  const[formNews, setformNews] = useState(false)

  const[posts, setPosts] = useState([])
  
  const header = 'Новость 1'
  const image = ''
  const content = 'Компью́тер (англ. computer, МФА: [kəmˈpjuː.tə(ɹ)][1] — «вычислитель», от лат. computare — считать, вычислять[2]) — термин, пришедший в русский язык из иноязычных (в основном — английских) источников, одно из названий электронной вычислительной машины.[3] Используется в данном смысле в русском литературном языке,[4][5] научной, научно-популярной литературе.[6]'

  useEffect(() => {
    const api = 'http://localhost:9001/new'

    fetch(api, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(result => result.json())
    .then((result) => {
      let mas = []
      result.forEach((element, index) => {
        mas.push(<Post key={index} id={element._id} header={element.header} image={element.image} content={element.content} />)
      });
      setPosts(mas)
      console.log(result)
    })
  }, [])


  return (
    <div className="News">
        <div className='News__admin_controls'>
          {IsAuth ? <button onClick={() => setformNews(true)}>Добавить новость</button>: null}
          {console.log(IsAuth)}
        </div>
        {
          formNews ? <NewsFormAdd setformNews={setformNews} /> : null
        }
        {posts}
    </div>
  );
}

export default News;
