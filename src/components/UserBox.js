import React from 'react';
import './UserBox.css';

function UserBox({setModalBox}) {
  return (
    <div className="UserBox">
        <p>Гость</p>
        <img src='' />
        <ul>
            <li onClick={()=>setModalBox(true)}>Регистрация</li>
            <li onClick={()=>setModalBox(true)}>Вход</li>
        </ul>
    </div>
  );
}

export default UserBox;
