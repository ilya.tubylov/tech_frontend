import React from 'react';
import './Header.css';
import Logo from './Logo';
import Nav from './Nav';
import UserBox from './UserBox';

function Header({setModalBox}) {
  return (
    <div className="Header">
      <Logo />
      <Nav />
      <UserBox setModalBox={setModalBox} />
    </div>
  );
}

export default Header;
