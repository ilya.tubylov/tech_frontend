import React from 'react';
import './Post.css';

function Post({id, header, image, content, IsAuth}) {
  function Delete(){
    const post_id = id
    console.log(id)
    const api = 'http://localhost:9001/deletepost'

    fetch(api, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id: post_id})
    })
    .then(result => result.json())
    .then((result) => {
      console.log(result)
    })

  }
  return (
    <div id={id} className="Post">
        <div className='Post__image'>
            <img src={image} />
        </div>
        <div className='Post__info'>
            <h2>{header}</h2>
            <p>{content}</p>
        </div>
       <button onClick={Delete}>X</button>
    </div>
  );
}

export default Post;
