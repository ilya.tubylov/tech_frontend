import React, {useState} from 'react';
import News from '../components/News';
import './MainPage.css';

function MainPage({IsAuth}) {

  return (
    <div className="MainPage">
        <h1>Центр детского и молодёжного инновационного творчества Технотроник</h1>
        <News IsAuth={IsAuth} />
    </div>
  );
}

export default MainPage;
